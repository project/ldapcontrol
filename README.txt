ABOUT
-----
LDAPcontrol module provides an API to control data structures on a LDAP server.

The module is developed and tested with OpenLDAP 2.4 (http://www.openldap.org/)
on Centos 7.

INSTALLATION
------------
Just add this module to the modules folder and enable it.

NO UPGRADE PATH FROM D6 VERSION
-------------------------------
This module is very special with a low usage so providing an upgrade path would
be a big work. Especially if you need removed user operations check out module
LDAPControl UserSync: https://www.drupal.org/project/ldapcontrol_usersync

AUTHOR
------
Carsten Logemann (https://drupal.org/user/218368)
Some code especially in LDAPControlInterface.inc is originally based on
LDAPinterface.inc in ldap_prov.module by Miglius Alaburda.
