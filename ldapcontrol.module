<?php

/**
 * @file
 * Controls an user Database in a LDAP directory server.
 */

/**
 * Initiates LDAP object.
 *
 * @return bool
 *   Connection success.
 */
function _ldapcontrol_ldapobject() {
  global $conf;
  $_ldapcontrol_ldap = FALSE;

  require_once drupal_get_path('module', 'ldapcontrol') . '/includes/LDAPControlInterface.inc';
  $tls = FALSE;
  if (isset($conf['ldapcontrol_tls'])) {
    $tls = $conf['ldapcontrol_tls'];
  }
  $bind_dn = '';
  if (isset($conf['ldapcontrol_bind_dn'])) {
    $bind_dn = $conf['ldapcontrol_bind_dn'];
  }

  $bind_pw = '';
  if (isset($conf['ldapcontrol_bind_pw'])) {
    $bind_pw = $conf['ldapcontrol_bind_pw'];
  }

  if (isset($conf['ldapcontrol_server'])
    && isset($conf['ldapcontrol_port'])
    && $bind_pw != '') {
    $_ldapcontrol_ldap = new LDAPControlInterface();
    $_ldapcontrol_ldap->setOption('server', $conf['ldapcontrol_server']);
    $_ldapcontrol_ldap->setOption('port', isset($conf['ldapcontrol_port']));
    $_ldapcontrol_ldap->setOption('tls', $tls);
    $_ldapcontrol_ldap->setOption('binddn', $bind_dn);
    $_ldapcontrol_ldap->setOption('bindpw', $bind_pw);

    // Currently hardcoded option:
    $_ldapcontrol_ldap->setOption('version', 3);

    return $_ldapcontrol_ldap;
  }
  return FALSE;
}

/**
 * API for ldap operations.
 */
function _ldapcontrol_op($op, $dn, $data = array()) {
  $_ldapcontrol_ldap = _ldapcontrol_ldapobject();
  $ldap_op = FALSE;
  $return = array();

  if (!$_ldapcontrol_ldap->connect()) {
    watchdog('ldapcontrol_ldapoperation', 'Could not bind.', array(), WATCHDOG_ERROR);
    $return['op'] = FALSE;
    return $return;
  }

  switch ($op) {
    case 'create':
      if ($_ldapcontrol_ldap->createEntry($dn, $data)) {
        $ldap_op = TRUE;
      }
      break;

    case 'update':
      if ($_ldapcontrol_ldap->updateEntry($dn, $data)) {
        $ldap_op = TRUE;
      }
      break;

    case 'delete':
      if ($_ldapcontrol_ldap->deleteEntry($dn)) {
        $ldap_op = TRUE;
      }
      break;

    case 'read':
      $return['read'] = array();
      $read = $_ldapcontrol_ldap->readEntry($dn, $data);

      if ($read) {
        $ldap_op = TRUE;
        $return['read'] = $read;
      }
      break;

    case 'search':
      $search = array();
      $search = $_ldapcontrol_ldap->search($dn, '(' . $data['search'] . ')', $data['attributes']);

      if ($search) {
        $ldap_op = TRUE;
      }
      $return['search'] = $search;

      break;

  }

  if (variable_get('ldapcontrol_debug', 0)) {
    if (user_access('administer ldapcontrol')) {
      $debug = "<h1>op[$op] dn: $dn</h1>";
      $debug .= '<code>' . var_export($read, TRUE) . '</code>';
      drupal_set_message($debug, 'warning');
    }
  }
  $_ldapcontrol_ldap->disconnect();
  $return['op'] = $ldap_op;
  return $return;
}

/**
 * Helper function to writw watchdog messages.
 *
 * @param array $data
 *   An associative array containing:
 *   - op: error, warning or status.
 *   - msg: A message string.
 */
function _ldapcontrol_msg(array $data = array()) {
  if ($data['op'] == 'error') {
    watchdog('ldapcontrol', $data['msg'], NULL, WATCHDOG_ERROR, NULL);
  }
  if ($data['op'] == 'warning') {
    watchdog('ldapcontrol', $data['msg'], NULL, WATCHDOG_WARNING, NULL);
  }
}

/**
 * Implements hook_menu().
 */
function ldapcontrol_menu() {
  $items = array();

  $items['admin/config/ldapcontrol'] = array(
    'title' => 'LDAP Control',
    'description' => 'Administer LDAP Control',
    'position' => 'right',
    'weight' => -5,
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer ldapcontrol'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );
  $items['admin/config/ldapcontrol/settings'] = array(
    'title' => 'LDAP Control check and settings',
    'description' => 'Check LDAP Control connection and change global settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ldapcontrol_settings_form'),
    'access arguments' => array('administer ldapcontrol'),
    'file' => 'includes/ldapcontrol.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function ldapcontrol_permission() {
  return array(
    'administer ldapcontrol' => array(
      'title' => t('Administer LDAPcontrol'),
      'description' => t('Primary used for enabling and usage of debug information.'),
    ),
  );
}
