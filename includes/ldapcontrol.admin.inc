<?php

/**
 * @file
 * LDAPcontrol admin page callback.
 */

/**
 * Form builder function.
 */
function ldapcontrol_settings_form() {
  global $conf;

  if (isset($conf['ldapcontrol_server']) && isset($conf['ldapcontrol_port'])) {
    $_ldapcontrol_ldap = _ldapcontrol_ldapobject();

    if ($_ldapcontrol_ldap->connect()) {
      $msg = array(
        'status' => 'status',
        'message' => t('Authentication with the LDAP server succeeded.'),
      );
    }
    else {
      $msg = array(
        'status' => 'error',
        'message' => t('Authentication with the LDAP server failed.'),
      );
    }

    drupal_set_message($msg['message'], $msg['status']);

  }
  else {
    $msg = t('No LDAP credentials available in settings.php');
    drupal_set_message($msg, "warning");
  }

  $desc = t('Show LDAP data for configuration debugging. Permission "administer ldapcontrol" is needed.');
  $form['ldapcontrol_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable debugging'),
    '#description' => $desc,
    '#default_value' => variable_get('ldapcontrol_debug', 0),
  );

  return system_settings_form($form);
}
