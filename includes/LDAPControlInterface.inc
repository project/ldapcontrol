<?php

/**
 * @file
 * LDAPControlInterface class definition.
 *
 * Originally based on file LDAPControlInterface.inc by by Miglius Alaburda.
 */

/**
 * This class is providing an API to communicate with php ldap functions.
 *
 * Read more on "http://php.net/manual/en/ref.ldap.php".
 */
class LDAPControlInterface {
  public $connection;

  /**
   * Set Option.
   *
   * @params string $option
   *   The option to be set.
   *
   * @params string $value
   *   The value of this option.
   */
  public function setOption($option, $value) {
    switch ($option) {
      case 'server':
        $this->server = $value;
        break;

      case 'port':
        $this->port = $value;
        break;

      case 'tls':
        $this->tls = $value;
        break;

      case 'binddn':
        $this->binddn = $value;
        break;

      case 'bindpw':
        $this->bindpw = $value;
        break;

      case 'version':
        $this->version = $value;
        break;

    }
  }

  /**
   * Get Option.
   *
   * @param string $option
   *   The option to get.
   *
   * @return string
   *   The value of this option.
   */
  public function getOption($option) {
    $ret = '';

    switch ($option) {
      case 'version':
        $ret = $this->version;
        break;

      case 'port':
        $ret = $this->port;
        break;

      case 'tls':
        $ret = $this->tls;
        break;

      case 'binddn':
        $ret = isset($this->binddn) ? $this->binddn : NULL;
        break;

      case 'bindpw':
        $ret = isset($this->bindpw) ? $this->bindpw : NULL;
        break;

    }
    return $ret;
  }

  /**
   * Connect to LDAP directory.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function connect() {
    $ret = FALSE;
    $this->disconnect();

    if ($this->connectAndBind($this->binddn, $this->bindpw)) {
      $ret = TRUE;
    }

    return $ret;
  }

  /**
   * Init connection to a LDAP directory.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function initConnection() {
    if (!$con = ldap_connect($this->server, $this->port)) {
      $watchdog = array('@server' => $this->server, '@port' => $this->port);
      watchdog('ldapcontrol', 'LDAP Connect failure to @server:@port', $watchdog, WATCHDOG_ERROR);
      return FALSE;
    }

    ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($con, LDAP_OPT_REFERRALS, 0);

    // TLS encryption contributed by sfrancis@drupal.org.
    if ($this->tls) {
      ldap_get_option($con, LDAP_OPT_PROTOCOL_VERSION, $vers);
      if ($vers == -1) {
        watchdog('ldapcontrol', 'Could not get LDAP protocol version.', array(), WATCHDOG_ERROR);
        return FALSE;
      }
      if ($vers != 3) {
        watchdog('ldapcontrol', 'Could not start TLS, only supported by LDAP v3.', array(), WATCHDOG_ERROR);
        return FALSE;
      }
      elseif (!function_exists('ldap_start_tls')) {
        watchdog('ldapcontrol', 'Could not start TLS. It does not seem to be supported by this PHP setup.', array(), WATCHDOG_ERROR);
        return FALSE;
      }
      elseif (!ldap_start_tls($con)) {
        $watchdog = array(
          '%errno' => ldap_errno($con),
          '%error' => ldap_error($con),
        );
        watchdog('ldapcontrol', 'Could not start TLS. (Error %errno: %error).', $watchdog, WATCHDOG_ERROR);
        return FALSE;
      }
    }
    $this->connection = $con;
    return TRUE;
  }

  /**
   * Connct and bind to LDAP directory.
   *
   * @params string $bind_dn
   *   The distinguished name of the LDAP entity to use for bind.
   *
   * @params string $bind_pw
   *   The password of the LDAP entity to use for bind.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function connectAndBind($bind_dn = '', $bind_pw = '') {
    $this->initConnection();

    $con = $this->connection;
    if (!$this->bind($bind_dn, $bind_pw)) {
      $watchdog = array(
        '%user' => $bind_dn,
        '%errno' => ldap_errno($con),
        '%error' => ldap_error($con),
      );
      watchdog('ldapcontrol', 'LDAP Bind failure for user %user. Error %errno: %error', $watchdog);
      return NULL;
    }

    return $con;
  }

  /**
   * Bind LDAP directory.
   *
   * @params string $bind_dn
   *   The distinguished name of the LDAP entity to use for bind.
   *
   * @params string $bind_pw
   *   The password of the LDAP entity to use for bind.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function bind($bind_dn, $bind_pw) {
    ob_start();
    set_error_handler(array('LDAPControlInterface', 'voidErrorHandler'));
    $ret = ldap_bind($this->connection, $bind_dn, $bind_pw);
    restore_error_handler();

    ob_end_clean();

    return $ret;
  }

  /**
   * Disconnect from LDAP directory.
   */
  public function disconnect() {
    if ($this->connection) {
      ldap_unbind($this->connection);
      $this->connection = NULL;
    }
  }

  /**
   * Search on LDAP directory.
   *
   * Warning: This function returns its entries with lowercase attribute names.
   *
   * @params string $base_dn
   *   The distinguished name of a LDAP "path" to search.
   *
   * @params string $filter
   *   The search filter can be simple or advanced, using boolean operators in
   *   the format described in the LDAP documentation.
   *
   * @params array $attributes
   *   An optional array of the required attributes, e.g. array("mail", "cn").
   *   Needed to retrieve additional Information like "memberOf". For example:
   *   to get all others and "memberOf" use array('*', 'memberOf').
   *   Note that the "dn" is always returned irrespective of which attributes
   *   types are requested.
   *   Using this parameter is much more efficient than the default action
   *   (which is to return all attributes and their associated values). The use
   *   of this parameter should therefore be considered good practice.
   *
   * @return mixed
   *   Returns a search result identifier or FALSE on error.
   */
  public function search($base_dn, $filter, $attributes = array()) {
    $ret = array();

    // For the AD the '\,' should be replaced by the '\\,' in the search filter.
    $filter = preg_replace('/\\\,/', '\\\\\,', $filter);

    set_error_handler(array('LDAPControlInterface', 'voidErrorHandler'));
    $x = @ldap_search($this->connection, $base_dn, $filter, $attributes);
    restore_error_handler();

    if ($x && ldap_count_entries($this->connection, $x)) {
      $ret = ldap_get_entries($this->connection, $x);
    }
    return $ret;
  }

  /**
   * Read entry on LDAP directory.
   *
   * Warning: This function returns its entries with lowercase attribute names.
   *
   * @params string $dn
   *   The distinguished name of an LDAP entity.
   *
   * @params array $attributes
   *   An optional array of the required attributes, e.g. array("mail", "cn").
   *   Needed to retrieve additional Information like "memberOf". For example:
   *   to get all others and "memberOf" use array('*', 'memberOf').
   *   Note that the "dn" is always returned irrespective of which attributes
   *   types are requested.
   *   Using this parameter is much more efficient than the default action
   *   (which is to return all attributes and their associated values). The use
   *   of this parameter should therefore be considered good practice.
   *
   * @params string $filter
   *   Default filter is set to "objectClass=*" to serach for all classes. If
   *   you know which entry types are used on the directory server, you might
   *   use an appropriate filter such as objectClass=inetOrgPerson.
   *
   * @return mixed
   *   Returns a search result identifier or FALSE on error.
   */
  public function readEntry($dn, $attributes = array(), $filter = 'objectClass=*') {
    set_error_handler(array('LDAPControlInterface', 'voidErrorHandler'));
    $result = ldap_read($this->connection, $dn, 'objectClass=*', $attributes, 0, 1);
    $entries = ldap_get_entries($this->connection, $result);
    restore_error_handler();
    return $entries[0];
  }

  /**
   * Update entry on LDAP directory.
   *
   * @params string $dn
   *   The distinguished name of an LDAP entity.
   *
   * @params array $entry
   *   An array that specifies the information about the entry. The values in
   *   the entries are indexed by individual attributes. In case of multiple
   *   values for an attribute, they are indexed using integers starting with 0.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function updateEntry($dn, $entry) {
    foreach ($entry as $key => $cur_val) {
      if ($cur_val == '') {
        unset($entry[$key]);
        $old_value = $this->retrieveAttribute($dn, $key);
        if (isset($old_value)) {
          ldap_mod_del($this->connection, $dn, array($key => $old_value));
        }
      }
      if (is_array($cur_val)) {
        foreach ($cur_val as $mv_key => $mv_cur_val) {
          if ($mv_cur_val == '') {
            unset($entry[$key][$mv_key]);
          }
          else {
            $entry[$key][$mv_key] = $mv_cur_val;
          }
        }
      }
    }

    return ldap_modify($this->connection, $dn, $entry);
  }

  /**
   * Add entry to LDAP directory.
   *
   * @params string $dn
   *   The distinguished name of an LDAP entity.
   *
   * @params array $entry
   *   An array that specifies the information about the entry. The values in
   *   the entries are indexed by individual attributes. In case of multiple
   *   values for an attribute, they are indexed using integers starting with 0.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function createEntry($dn, $entry) {
    set_error_handler(array('LDAPControlInterface', 'voidErrorHandler'));
    $ret = ldap_add($this->connection, $dn, $entry);
    restore_error_handler();

    return $ret;
  }

  /**
   * Modify the name of an LDAP entity.
   *
   * @params string $dn
   *   The distinguished name of an LDAP entity.
   *
   * @params string $newrdn
   *   The new RDN.
   *
   * @params string $newparent
   *   The new parent/superior entry.
   *
   * @params bool $deleteoldrdn
   *   If TRUE the old RDN value(s) is removed, else the old RDN value(s) is
   *   retained as non-distinguished values of the entry.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function renameEntry($dn, $newrdn, $newparent, $deleteoldrdn) {
    set_error_handler(array('LDAPControlInterface', 'voidErrorHandler'));
    $ret = ldap_rename($this->connection, $dn, $newrdn, $newparent, $deleteoldrdn);
    restore_error_handler();

    return $ret;
  }

  /**
   * Delete an LDAP Entity.
   *
   * @param string $dn
   *   The distinguished name of an LDAP entity.
   *
   * @return bool
   *   Returns TRUE on success or FALSE on failure.
   */
  public function deleteEntry($dn) {
    set_error_handler(array('LDAPControlInterface', 'voidErrorHandler'));
    $ret = ldap_delete($this->connection, $dn);
    restore_error_handler();

    return $ret;
  }

  /**
   * Delete a single attribute of an LDAP entity.
   *
   * @params string $dn
   *   The distinguished name of an LDAP entity.
   *
   * @params string $attribute
   *   Which attribute to delete.
   */
  public function deleteAttribute($dn, $attribute) {
    ldap_mod_del($this->connection, $dn, array($attribute => array()));

  }

  /**
   * Void error handler.
   */
  static public function voidErrorHandler($p1, $p2, $p3, $p4, $p5) {
    // Do nothing.
  }

}
